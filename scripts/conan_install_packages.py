#!/usr/bin/env python3
import argparse
import subprocess
import sys

def install_package(package_ref, args):
  print(f'installing {package_ref}, profile build: {args.profile_build}, profile host: {args.profile_host}, build type: {args.build_type}')

  command = [
    'conan', 'install', f'{package_ref}@',
    '--profile:build', f'{args.profile_build}', '--profile:host', f'{args.profile_host}',
    '--settings:build', 'build_type=Release', '--settings:host', f'build_type={args.build_type}'
  ]

  if args.build_mode:
    command.append('--build')
    command.append('missing')
    command.append('--update')

  print(f'command: {command}')

  # Prevent having print() logs after run()
  sys.stdout.flush()

  subprocess.run(command, check=True)


parser = argparse.ArgumentParser()

parser.add_argument('--profile_build', help='Conan profile to use in the build context', required=True)
parser.add_argument('--profile_host', help='Conan profile to use in the host context', required=True)
parser.add_argument('--build_type', help='Build type for the host context, like Debug, Release', required=True)
parser.add_argument('--build_mode', help='If True, will pass --build missing --update to conan install', action='store_true', required=False)

args = parser.parse_args()

install_package('boost/1.72.0', args)
install_package('catch2/2.13.9', args)
