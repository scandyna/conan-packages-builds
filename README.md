# conan-packages-builds

Builds some conan packages using personal profiles from [conan-config](https://gitlab.com/scandyna/conan-config).

Because the combination of binaries is big, some personal builds are made from here,
avoiding to rebuiuld them every time.

# Usage

## Remove conancenter remote

[conan-center](https://conan.io/center/) provides packages for most of the popular libraries.

They also continuously update versions, fix issues and are currently migrating recipes to conan V2.

This can require to rebuild packages, and also can block anything due to an issue.
To avoid this, remove the conancenter remote:

```shell
conan remote remove conancenter
```

## Add the remote

The packages are pushed to a central packages regsitry of [this project](https://gitlab.com/scandyna/packages).

Add the remote:
```shell
conan remote add scandyna https://gitlab.com/api/v4/projects/25668674/packages/conan
```
