from conan import ConanFile

class TestsRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps", "VirtualBuildEnv"

    def requirements(self):
        self.requires("boost/1.72.0")

    def build_requirements(self):
        self.test_requires("MdtCMakeModules/0.19.1@scandyna/testing")
        self.test_requires("catch2/2.13.9")
