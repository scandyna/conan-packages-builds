
cmake_minimum_required(VERSION 3.18)

project(ConanPackagesBuildTests LANGUAGES CXX)

set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMAKE_VISIBILITY_INLINES_HIDDEN YES)

find_package(Boost REQUIRED)
find_package(Catch2 REQUIRED)
find_package(MdtCMakeModules REQUIRED)

#######################
# Windows specific
#######################

# On Windows, RPATH do not exist
# To be able to run tests, we have to put all binaries in the same directory
# TODO: should remove to face the bug !
if(WIN32)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
endif()

# Workaround for the "too many sections" error on some MinGW compiler
# See: https://stackoverflow.com/questions/16596876/object-file-has-too-many-sections
if(MINGW)
  add_compile_options(-Wa,-mbig-obj)
endif()

#######################
# Sources
#######################

enable_testing()

add_subdirectory(SimpleApp)
